const test = require('ava')

const api = require('../apiTest')
let idP = 0

// create new 
test('Testing creating a new projection', async t => {
  const {
    body
  } = await api.post('/createProjection')
    .send(
        {
            "projection_name" : "Spiderman",
            "projection_time" : "22:00",
            "projection_date":"2022-01-01",
            "place_id" : "1",
            "category_id" : "2"
        }
        
    )
  t.assert(body.projection_id, 'Projection is not created!')
  idP = body.projection_id
})

/////////////////////////////////////////////////////////////
test('Testing getById existing projection', async t => {
    const {
      body
    } = await api.get('/getProjection/'+idP)
    t.is(body.projection, idP)
})

test('Testing getById non existing', async t => {
    const {
      body
    } = await api.get('/getProjection/333333').catch(err =>{
        t.assert(err, "OK")
    })
})

/////////////////////////////////////////////////////////////
test('Testing getProjectionByType ', async t => {
    const {
      body
    } = await api.get('/getProjectionByType/1')
   t.is(body[0].typep_name, 'CINEMA')
})

/////////////////////////////////////////////////////////////
test('Testing getProjectionByCategory', async t => {
    const {
      body
    } = await api.get('/getProjectionByCategory/2')
   t.is(body[0].category_name, "HOROR")
})

/////////////////////////////////////////////////////////////
test('Testing getProjectionForDay', async t => {
    const {
      body
    } = await api.get('/getProjectionForDay/2022-01-01')
   t.is(body[0])
})
/////////////////////////////////////////////////////////////
test('Testing getProjectionsFromPlace', async t => {
    const {
      body
    } = await api.get('/getProjectionsFromPlace/1')
   t.is(body[0])
})

/////////////////////////////////////////////////////////////

/////////////////////////////////////////////////////////////
test('Testing updating a projection', async t => {
    const {
      body
    } = await api.post('/editProjection')
      .send(
          {
              "id":id,
              "name":"Cinestar Joker Edited"
          }
      )
    t.is(body.state, 'Updated sucessfully!')
  })
  
/////////////////////////////////////////////////////////////
test('Testing DELETE existing ', async t => {
    const {
      body
    } = await api.get('/deleteProjection/'+id)
    t.is(body, 'Sucessfully deleted')
    t.is(body.row_count, 1)
})

test('Testing DELETE non-existing ', async t => {
    const {
      body
    } = await api.get('/deleteProjection/'+id)
   t.is(body.row_count, 0)
})


