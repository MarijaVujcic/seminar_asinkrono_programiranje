const test = require('ava')

const api = require('../apiTest')
const autorRepo = require('../repo/place')
let id = 0

// create new 
test('Testing creating a new place', async t => {
  const {
    body
  } = await api.post('/createPlace')
    .send(
        {
            "name":"Cinestar Joker",
            "location":"1",
            "wdays": [1,2,3,4,5],
            "typeId": "1"
        }
    )
  t.assert(body.place_id, 'Place is not created!')
  id = body.place_id
})

test('Testing creating a existing place', async t => {
    const {
      body
    } = await api.post('/createPlace')
      .send(
          {
              "name":"Cinestar Joker",
              "location":"3",
              "wdays": [1,2,3,4,5],
              "typeId": "2"
          }
      )
    t.assert(!body.place_id, 'Place is created!')
  })

/////////////////////////////////////////////////////////////
test('Testing getById existing ', async t => {
    const {
      body
    } = await api.get('/getPlace/'+id)
    t.is(body.place_id, id)
})

test('Testing getById non existing', async t => {
    const {
      body
    } = await api.get('/getPlace/333333').catch(err =>{
        t.assert(err, "OK")
    })
})

/////////////////////////////////////////////////////////////
test('Testing get all working days ', async t => {
    const {
      body
    } = await api.get('/getAllWorkingDays/'+id)
   t.is(body[0].wday_name, 'MON')
})
test('Testing get all working days 2', async t => {
    const {
      body
    } = await api.get('/getAllWorkingDays/'+id)
   t.is(body.length, 3)
})
/////////////////////////////////////////////////////////////
test('Testing getAllPlacesByTypeAndWorkDay', async t => {
    const {
      body
    } = await api.get('/getAllPlacesByTypeAndWorkDay/'+1+'/'+'1')
   t.assert(body[0], "Ne posroji nitko a trebalo bi")
})

/////////////////////////////////////////////////////////////
test('Testing getAllPlacesByType', async t => {
    const {
      body
    } = await api.get('/getAllPlacesByType/'+1)
   t.is(body[0].typep_name, 'CINEMA')
})
/////////////////////////////////////////////////////////////
test('Testing getAllPlacesOfLocation', async t => {
    const {
      body
    } = await api.get('/getAllPlacesOfLocation/'+1)
   t.is(body[0].town, 'Split')
})

/////////////////////////////////////////////////////////////
test('Testing getAllPlacesByLocationAndType', async t => {
    const {
      body
    } = await api.get('/getAllPlacesByLocationAndType'+1).send(
        {
          "location_id": "1",
          "typeId": "1"
        }
    )
   t.is(body.length > 0)
})

/////////////////////////////////////////////////////////////
test('Testing updating a new place', async t => {
    const {
      body
    } = await api.post('/editPlace')
      .send(
          {
              "id":id,
              "name":"Cinestar Joker Edited"
          }
      )
    t.is(body.state, 'Updated  place sucessfully!')
  })
  
/////////////////////////////////////////////////////////////
test('Testing DELETE existing ', async t => {
    const {
      body
    } = await api.get('/deletePlace/'+id)
    t.is(body, 'Sucessfully deleted')
})

test('Testing DELETE non-existing ', async t => {
    const {
      body
    } = await api.get('/deletePlace/3333')
   // t.is(body, 'Sucessfully deleted')
})


