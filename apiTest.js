const test = require('ava')
require('dotenv-safe').config()

const supertest = require('supertest')

const { pool } = require('./db')
const app = require('./app')
const CustomError = require('./helper/custom_error')

const api = supertest(app.callback())

test.after('cleanup', async t => {
  try{
    await pool.end()}
  catch (err) {
    if (err instanceof CustomError && err.status < 500) {
      const { status, message } = err
      ctx.status = status
      ctx.body = {
        message,
        err: err.err,
      }
    } else {
      throw err
    }
  }
})


module.exports = api
