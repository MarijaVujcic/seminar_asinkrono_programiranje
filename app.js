require('dotenv-safe').config()
const Koa = require('koa')
const bodyParser = require('koa-bodyparser')
const CustomError = require('./helper/custom_error')

const app = new Koa()
app.use(bodyParser())

app.use(async (ctx, next) => {
  try {
    await next()
  } catch (err) {
    if (err instanceof CustomError && err.status < 500) {
      const { status, message } = err
      ctx.status = status
      ctx.body = {
        message,
        err: err.err,
      }
    } else {
      throw err
    }
  }
})

app.use(require('./routes/place').routes())
app.use(require('./routes/projection').routes())


module.exports = app
