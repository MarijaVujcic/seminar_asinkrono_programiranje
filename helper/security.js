const { checkCreds } = require('./util')
const jwt = require('jsonwebtoken')

async function basic (ctx, next) {
    const token = ctx.header['basic-authorization']
    const revealT = jwt.verify(token, process.env.JWT_SECRET)
    //const result = Buffer.from(revealT, 'base64').toString()
    const {username, password} = revealT
    if (checkCreds(username, password)) {
      ctx.status = 401
      ctx.body = { working: 'Wrong credentials' }
      return
    }
    return next()
  }
  

module.exports = {
    basic
  }
  
