class CustomError extends Error {
    constructor (message, status, err) {
      super(message)
      this.message = message
      this.status = status
      this.err = err
      Error.captureStackTrace(this, CustomError)
    }
  }
  
  module.exports = CustomError
  