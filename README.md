////////////////////////////////////////////////////////////////////////////////////////////
INSERT AKCIJE ZA VJEZBU
////////////////////////////////////////////////////////////////////////////////////////////
INSERT INTO places (place_name, location) VALUES ('Kino skalice', 'Put Skalica 52'); 
INSERT INTO places (place_name, location) VALUES ('Kazaliste skalice', 'Put Skalica 56'); 


INSERT INTO place_type (place_id, typeP_id) VALUES ('1', '2'); 
INSERT INTO place_type (place_id, typeP_id) VALUES ('2', '2'); 




////////////////////////////////////////////////////////////////////////////////////////////
DOLJE NAVEDENE AKCIJE IZVRSAVA ADMIN ZA DODAVANJE DEFINIRANIH KATEGORIJA
ZADATAK NE TRAZI ROUTE ZA STVARANJE ISTIH, PA SE MANUALNO DODAVAJU
////////////////////////////////////////////////////////////////////////////////////////////


INSERT INTO typeP (typeP_name) VALUES ('CINEMA');
INSERT INTO typeP (typeP_name) VALUES ('THEATER');

INSERT INTO working_days (wday_name) VALUES ('MON');
INSERT INTO working_days (wday_name) VALUES ('TUE');
INSERT INTO working_days (wday_name) VALUES ('WEN');
INSERT INTO working_days (wday_name) VALUES ('THU');
INSERT INTO working_days (wday_name) VALUES ('FRI');
INSERT INTO working_days (wday_name) VALUES ('SAT');
INSERT INTO working_days (wday_name) VALUES ('SUN');


INSERT INTO location (town) VALUES ('Split');
INSERT INTO location (town) VALUES ('Zagreb');
INSERT INTO location (town) VALUES ('Dubrovnik');
INSERT INTO location (town) VALUES ('Rijeka');

INSERT INTO category_types (category_name) VALUES ('HOROR');
INSERT INTO category_types (category_name) VALUES ('DRAMA');
INSERT INTO category_types (category_name) VALUES ('KOMEDIJA');
INSERT INTO category_types (category_name) VALUES ('TRILER');


////////////////////////////////////////////////////////////////////////////////////////////
psql -h localhost -p5432 -U postgres -w
///////////////////////////////////////////////////////////////////////////////////////////
