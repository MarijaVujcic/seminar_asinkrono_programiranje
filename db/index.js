const {Pool} = require('pg')
require('dotenv-safe').config()


const pool = new Pool({
  connectionString: process.env.DATABASE_URL
})

async function transaction (fn) {
  const client = await pool.connect()
  let reusable = false

  try {
    await client.query('BEGIN')
    const result = await fn(client)
    await client.query('COMMIT')
    return result
  } catch (e) {
    await client.query('ROLLBACK')
    throw e
  } finally {
    client.release()
  }
}

pool.query('SELECT NOW()')
.then(() => {
  console.log('Connected to database!')
})
.catch(err => {
  console.error(err)
  process.exit(1)
})

module.exports = {
  pool,
  transaction
}
