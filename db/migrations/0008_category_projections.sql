
CREATE TABLE category_projections (
  category_projections_id serial NOT NULL PRIMARY KEY,
  projection_id int REFERENCES projections (projection_id) ON UPDATE CASCADE ON DELETE CASCADE
, category_id int REFERENCES category_types (category_id) ON UPDATE CASCADE ON DELETE CASCADE
);