
CREATE TABLE IF NOT EXISTS working_days (
    wday_id SERIAL PRIMARY KEY NOT NULL,
    wday_name TEXT
);
