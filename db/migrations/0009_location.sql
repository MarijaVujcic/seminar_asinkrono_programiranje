CREATE TABLE IF NOT EXISTS location (
  location_id serial PRIMARY KEY,
  town TEXT
);
