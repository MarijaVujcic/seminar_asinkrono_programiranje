
CREATE TABLE IF NOT EXISTS places (
  place_id serial PRIMARY KEY,
  place_name TEXT UNIQUE NOT NULL
);
