CREATE TABLE location_place (
  location_place_id serial NOT NULL PRIMARY KEY,
  place_id  int REFERENCES places (place_id) ON UPDATE CASCADE ON DELETE CASCADE
, location_id int REFERENCES location (location_id) ON UPDATE CASCADE ON DELETE CASCADE
);