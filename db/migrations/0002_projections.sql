-- Projections
CREATE TABLE IF NOT EXISTS projections (
  projection_id serial PRIMARY KEY,
  projection_time TEXT ,
  projection_date date,
  place_id int REFERENCES places (place_id),
  projection_name TEXT
);
