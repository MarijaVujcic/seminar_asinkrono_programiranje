
CREATE TABLE place_type (
  place_type_id serial NOT NULL PRIMARY KEY,
  place_id int REFERENCES places (place_id) ON UPDATE CASCADE ON DELETE CASCADE
, typeP_id int REFERENCES typeP (typP_id) ON UPDATE CASCADE ON DELETE CASCADE
);