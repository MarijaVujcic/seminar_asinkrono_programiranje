
CREATE TABLE IF NOT EXISTS category_types (
  category_id SERIAL PRIMARY KEY,
  category_name TEXT 
);
