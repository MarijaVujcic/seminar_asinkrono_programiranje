
const { Client } = require('pg')
const { migrate } = require('postgres-migrations')
require('dotenv-safe').config()

async function run () {
  const client = new Client({
    connectionString: process.env.DATABASE_URL,
  })

  await client.connect()
  try {
    await migrate({client}, 'C:\\Users\\PC\\Desktop\\FAKS5\\asikrono web programiranje\\seminar\\seminar_asinkrono_programiranje\\db\\migrations')
  } finally {
    client.end()
  }
}

run()
.catch(e => {
  console.error(e)
  process.exit(1)
})
