const Router = require('@koa/router')
const placeRepo = require('../repo/place')
const _ = require('lodash')
const joi = require('joi')
const jwt = require('jsonwebtoken')

const validator = require('../helper/validator')
const auth = require('../helper/security')
const {
    USERNAME,
    PASSWORD
} = require('../helper/util')
const router = new Router()

///////////////////////
router.get('/jwt-check', async function (ctx) {
    const token = jwt.sign({
        username: USERNAME,
        password: PASSWORD
    }, process.env.JWT_SECRET)
    ctx.body = {
        token
    }
})

////////////////////////////////////////////////////////////  

// ok dobavlja
router.get('/getAllPlaces',
    async ctx => {
        ctx.body = await placeRepo.getAll()
    }
)

// ok dobavlja
router.get('/getPlace/:id',
    async ctx => {
        id = ctx.request.params.id
        ctx.body = await placeRepo.getOne(id)
    }
)

// ok
router.post('/createPlace',
    validator.body({
        name: joi.string().required(),
        location: joi.string().required(),
        wdays: joi.array().required(),
        typeId: joi.string().required(),
    }),
    async ctx => {
        ctx.body = await placeRepo.create(ctx.request.body)
    }
)

router.post('/editPlaceMoreInfos', auth.basic,
    validator.body({
        id: joi.string().required(),
        location: joi.string().required(),
        wdays: joi.array().required(),
        typeId: joi.string().required(),
    }),
    async ctx => {
        ctx.body = await placeRepo.editMore(ctx.request.body)
    }
)

router.post('/editPlace', auth.basic,
    validator.body({
        id: joi.string().required(),
        name: joi.string().trim().required(),
    }),
    async ctx => {
        ctx.body = await placeRepo.updateOneInfo(ctx.v.body)
    }
)

// ok
router.delete('/deletePlace', auth.basic,
    validator.body({
        id: joi.string().trim().required()
    }),
    async ctx => {
        ctx.body = await placeRepo.deleteOne(ctx.v.body.id)
    }
)

// ok
// dobavlja sve radne dane za odredeno mjesto
router.get('/getAllWorkingDays/:id',
    async ctx => {
        ctx.body = await placeRepo.getWrokingDayForPlace(ctx.params.id)
    }
)

// ok
// dobaviti sva kazalista/kina koja rade odredeni dan
router.get('/getAllPlacesByTypeAndWorkDay/:idt/:wid',
    async ctx => {
        ctx.body = await placeRepo.getAllPlacesByTypeAndWorkDay(ctx.params.idt, ctx.params.wid)
    }
)

router.get('/getAllPlacesByType/:id',
    async ctx => {
        ctx.body = await placeRepo.getAllPlacesByType(ctx.params.id)
    }
)


// dobavi sve projekcije nekog mista tj place
router.get('/getProjectionsByPlace/:id',
    async ctx => {
        ctx.body = await placeRepo.getProjectionsFromPlace(ctx.params.id)
    }
)

// dobavi sve places s neke lokacije
router.get('/getAllPlacesOfLocation/:id',
    async ctx => {
        ctx.body = await placeRepo.getAllPlacesByLocation(ctx.params.id)
    }
)

router.get('/getAllPlacesByLocationAndType'),
    validator.body({
        location_id: joi.string().required(),
        typeId: joi.string().required(),
    }, async ctx => {
        ctx.body = await placeRepo.getAllPlacesByLocationAndType(ctx.v.body.location_id, ctx.v.body.typeId)
    })


module.exports = router