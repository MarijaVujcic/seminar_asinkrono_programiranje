const Router = require('@koa/router')
const projectionRepo = require('../repo/projection')
const _ = require('lodash')
const joi = require('joi').extend(require('@joi/date'));

const validator = require('../helper/validator')
const auth = require('../helper/security')

const router = new Router()
const schema = joi.date().utc().format(['YYYY/MM/DD', 'MM-DD-YYYY']);
const CustomError = require('../helper/custom_error')

// dobavlja sve info o svim projekcijama
router.get('/getAllProjections',
    async ctx => {
        ctx.body = await projectionRepo.getAll()
    }
)

router.get('/getProjection/:id',
    async ctx => {
        id = ctx.request.params.id
        ctx.body = await projectionRepo.getOne(id)
    }
)


// tu da mi ispise i informacije o placeu
router.get('/getProjectionByType/:id',
    async ctx => {
        id = ctx.request.params.id
        ctx.body = await projectionRepo.getProjectionsByType(id)
    }
)

router.get('/getProjectionByCategory/:id',
    async ctx => {
        id = ctx.params.id
        ctx.body = await projectionRepo.getProjectionByCategory(id)
 
    }
)

router.get('/getProjectionForDay',validator.body({
    whatDate: schema.required()
}),
    async ctx => {
        try{
        ctx.body = await projectionRepo.getProjectionForDay(ctx.v.body.whatDate)

        }catch(err){
            throw new CustomError('Get projection for day error', 401, err.message)
        }

    }
)

router.get('/getProjectionsFromPlace/:id',
    async ctx => {
        id = ctx.params.id
        ctx.body = await projectionRepo.getProjectionsByType(id)
    }
)

router.get('/getProjectionsFromLocation/:id',
    async ctx => {
        id = ctx.params.id
        ctx.body = await projectionRepo.getAllProjectionsByLocation(id)
    }
)

// add new user -- EVERYONE
router.post('/createProjection',
    validator.body({
        projection_name: joi.string().trim().required(),
        projection_time: joi.string().trim().required(),
        projection_date: schema.required(),
        place_id: joi.string().required(),
        category_id: joi.string().required(),

    }),
    async ctx => {
        ctx.body = await projectionRepo.create(ctx.request.body)
    }
)

router.post('/editProjection', auth.basic,
    validator.body({
        projection_name: joi.string().required(),
        place_id: joi.string().trim().required(),
        projection_id: joi.string().required(),
        time: joi.string().required(),
        projection_date: joi.string().required(),
    }),
    async ctx => {
            ctx.body = await projectionRepo.updateOne(ctx.v.body)
    }
)

router.delete('/deleteProjection/:id', auth.basic,
    async ctx => {
        ctx.body = await projectionRepo.deleteOne(ctx.params.id)
    }
)

module.exports = router