const {
    pool
} = require('../db')

const CustomError = require('../helper/custom_error')


///////////////////////////////////////////////////////////
// get all info
async function getAll() {
    try {
        const {
            rows: projections
        } = await pool.query('SELECT * FROM projections')
        return projections
    } catch (e) {
        throw new CustomError('Get all projections', 404, e.message)
    }
}

// get one specific
async function getOne(whatId) {
    try {
        const {
            rows: [projection]
        } = await pool.query('SELECT projections.*, pl.place_name FROM projections\
         INNER JOIN places AS pl ON pl.place_id = projections.place_id\
         WHERE projections.projection_id = $1', [whatId])
        return projection
    } catch (e) {
        throw new CustomError('Projection get error', 404, e.message)
    }
}

// create new from body data
async function create(body) {
    try {
        const {
            rows: [p]
        } = await pool.query(`
      INSERT INTO projections (projection_time, place_id, projection_name, projection_date)
      VALUES ($1, $2, $3, $4)
      RETURNING projection_id
    `, [body.projection_time, body.place_id, body.projection_name, body.projection_date])
        await pool.query(`
            INSERT INTO category_projections (projection_id, category_id)
            VALUES ($1, $2)
            `, [p.projection_id, body.category_id])
        return p
    } catch (e) {
        throw new CustomError('Creation of projection error', 404, e.message)
    }
}

// update specific one
async function updateOne(body) {
    // ako je name null ili genre pprebrise se, dobavi ranije ili nekako u cacheu
    try {
        const s = pool.query('UPDATE projections SET projection_time = $1, \
            place_id = $2, projection_name = $3, projection_date = $4\
            WHERE projection_id = $5', [body.time, body.place_id, body.projection_name,
                 body.projection_date, body.projection_id])
        return {
            "state": 'Updated sucessfully!'
        }

    } catch (e) {
        throw new CustomError('Update projection error', 404, e.message)
    }

}
// delete specific one
async function deleteOne(whatId) {
    try {
        return pool.query('DELETE FROM projections WHERE "projection_id" = $1', [whatId])
    } catch (err) {
        throw new CustomError('Delete projection error', 404, err.message)
    }
}

// vraca info o projekcijama prema tipu projekcije
async function getProjectionsByType(what) {
    try {
        const {
            rows: projection
        } = await pool.query('SELECT projections.*, typ.typeP_name, pl.place_name FROM projections \
        JOIN places AS pl ON pl.place_id = projections.place_id \
        INNER JOIN place_type as plt ON plt.place_id = pl.place_id  \
        INNER JOIN typeP AS typ ON plt.typeP_id = typ.typP_id \
        WHERE plt.typeP_id = $1', [what])
        return projection
    } catch (e) {
        throw new CustomError('Projection get error', 404, e.message)
    }
}

async function getProjectionByCategory(whatId) {
    try {
        const {
            rows: [projection]
        } = await pool.query('SELECT projections.*, c.category_name FROM projections \
        INNER JOIN category_projections AS cp \
        ON cp.projection_id = projections.projection_id \
        INNER JOIN category_types AS c ON cp.category_id = c.category_id \
        WHERE cp.category_id = $1', [whatId])
        return projection
    } catch (e) {
        throw new CustomError('Projection get error', 404, e.message)
    }
}


async function getProjectionForDay(whatdate) {
    try {
        const {
            rows: projection
        } = await pool.query('SELECT projections.* FROM projections \
        WHERE projections.projection_date = $1 ', [whatdate])
        return projection
    } catch (e) {
        throw new CustomError('Projection get error', 404, e.message)
    }
}

async function getAllProjectionsByLocation(location_id) {
    try {
        const {
            rows: places
        } = await pool.query('SELECT projections.*, l.town FROM projections  \
      INNER JOIN location_place AS lp \
      ON lp.place_id = projections.place_id \
      INNER JOIN location AS l on lp.location_id = l.location_id \
      WHERE lp.location_id = $1 AND l.location_id = $1',
            [location_id])
        return places
    } catch (e) {
        throw new CustomError('Get all places by type error', 404, e.message)
    }

}

module.exports = {
    getAll,
    getOne,
    create,
    deleteOne,
    updateOne,
    getProjectionsByType,
    getProjectionForDay,
    getProjectionByCategory,
    getAllProjectionsByLocation

}