const {
  pool
} = require('../db')

const CustomError = require('../helper/custom_error')

// get all
async function getAll() {
  try {
    const {
      rows: places
    } = await pool.query('SELECT * FROM places')
    return places
  } catch (e) {
    throw new CustomError('Get all places error', 404, e.message)
  }
}

// get one specific
async function getOne(whatId) {
  try {
    const {
      rows: [place]
    } = await pool.query('SELECT * FROM places WHERE "place_id" = $1', [whatId])
    return place
  } catch (e) {
    throw new CustomError('Get one place error', 404, e.message)
  }
}
// create new from body data
// TODO: dodati i u ostale dane
async function create(body) {
  try {
    const {
      rows: [p]
    } = await pool.query(`
      INSERT INTO places (place_name)
      VALUES ($1)
      RETURNING place_id
    `, [body.name]);

    await pool.query(`
      INSERT INTO place_type (typeP_id, place_id)
      VALUES ($1, $2)
    `, [body.typeId, parseInt(p.place_id)])
    await pool.query(`
      INSERT INTO location_place (location_id, place_id)
      VALUES ($1, $2)
    `, [body.location,parseInt(p.place_id),])

    let lst = body.wdays
    console.log(lst)
    for (let i = 0; i < lst.length; i++) {
      await pool.query(`
      INSERT INTO place_working_days (wday_id, place_id)
      VALUES ($1, $2)
    `, [parseInt(lst[i]), parseInt(p.place_id)])
    }

    return p
  } catch (e) {
    throw new CustomError('Create place error', 404, e.message)
  }
}

async function editMore(body) {
  try {
    await pool.query(`
      UPDATE place_type SET typeP_id = $1 WHERE place_id = $2`, [body.typeId, parseInt(body.id)])

    await pool.query(`
      UPDATE location_place SET location_id = $1 WHERE place_id = $2`, [body.location,parseInt(body.id)])

    let lst = body.wdays
    console.log(lst)
    for (let i = 0; i < lst.length; i++) {
      await pool.query(`
      UPDATE place_working_days wday_id = $1 WHERE place_id = $2`,
       [parseInt(lst[i]), parseInt(body.id)])
    }
    return 'Updated sucessfuly!'
  } catch (e) {
    throw new CustomError('Create place error', 404, e.message)
  }
}

// TODO: paziti da pobrisem sve reference
async function deleteOne(whatId) {
  try {
    await pool.query('DELETE FROM places WHERE "place_id" = $1', [whatId])
    return 'Sucessfully deleted'
  } catch (e) {
    throw new CustomError('Delete place error', 404, e.message)
  }
}

// update specific one
async function updateOneInfo(body) {
  try {
    const s = await pool.query('UPDATE places SET \
       place_name = $1 \
       WHERE place_id = $2', [body.name, body.id])
    return {
      "state": 'Updated  place sucessfully!'
    }

  } catch (e) {
    throw new CustomError('Update theater error', 404, e.message)
  }

}

// get all for working days wID
async function getAllWorkingDay(body) {
  try {
    const {
      rows: places
    } = await pool.query('SELECT * FROM places AS p \
    JOIN place_working_days AS wd \
    ON p.place_id = wd.place_id \
    WHERE wd.wday_id = $1 ',
      [body.place_id])
    return places

  } catch (e) {
    throw new CustomError('Get all places on work day error', 404, e.message)
  }

}

// get one for working day wID i Id
async function getWrokingDayForPlace(id) {
  try {
    const {
      rows: days
    } = await pool.query('SELECT wday_name FROM working_days AS wd \
      LEFT JOIN place_working_days AS pwd \
      ON wd.wday_id = pwd.wday_id \
      WHERE pwd.place_id = $1',
      [id])
    return days

  } catch (e) {
    throw new CustomError('Get places on specific work day', 404, e.message)
  }

}

// get alll type typeId
async function getAllPlacesByType(place_id) {
  try {
    const {
      rows: places
    } = await pool.query('SELECT places.*, typ.typeP_name FROM places \
    JOIN place_type AS pt ON pt.place_id = places.place_id \
    INNER JOIN typeP as typ ON pt.typeP_id = typ.typP_id\
    WHERE pt.typeP_id = $1',
      [place_id])
    return places
  } catch (e) {
    throw new CustomError('Get all places by type error', 404, e.message)
  }

}

// get all typeId working day
async function getAllPlacesByTypeAndWorkDay(id, wid) {
  try {
    const {
      rows: places
    } = await pool.query('SELECT * FROM \
    places p INNER JOIN place_type pt ON p.place_id = pt.place_id \
    INNER JOIN place_working_days wd ON p.place_id = wd.place_id \
    WHERE pt.typeP_id = $1 and wd.wday_id = $2',
      [id, wid])
    return places
  } catch (e) {
    throw new CustomError('Get all places by type and work day error', 404, e.message)
  }

}

async function getProjectionsFromPlace(whatId) {
  try {
    const {
      rows: [projection]
    } = await pool.query('SELECT * FROM projections \
      WHERE place_id = $1', [whatId])
    return projection
  } catch (e) {
    throw new CustomError('Projection get error', 404, 'Author not found')
  }
}

async function getAllPlacesByLocation(location_id) {
  try {
    const {
      rows: places
    } = await pool.query('SELECT * FROM places AS p \
    JOIN location_place AS lp \
    ON lp.place_id = p.place_id \
    WHERE lp.location_id = $1',
      [location_id])
    return places
  } catch (e) {
    throw new CustomError('Get all places by type error', 404, e.message)
  }
}

async function getAllPlacesByLocationAndType(location_id, typeId) {
  try {
    const {
      rows: places
    } = await pool.query('SELECT * FROM places AS p \
    JOIN location_place AS lp \
    ON lp.place_id = p.place_id \
    JOIN place_type AS pty \
    ON pty.place_id = p.place_id\
    WHERE pty.typeP_id = $1 AND lp.location_id = $2',
      [typeId, location_id])
    return places
  } catch (e) {
    throw new CustomError('Get all places by type error', 404, e.message)
  }

}

module.exports = {
  getAll,
  getOne,
  create,
  updateOneInfo,
  getAllWorkingDay,
  getAllPlacesByType,
  deleteOne,
  getWrokingDayForPlace,
  getAllPlacesByTypeAndWorkDay,
  getAllPlacesByLocation,
  getProjectionsFromPlace,
  getAllPlacesByLocationAndType,
  editMore
}