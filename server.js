const app = require('./app')
require('dotenv-safe').config()

const PORT = process.env.PORT

app.listen(PORT, err => {
  if (err) {
    console.error(err)
    return
  }
  console.log('Working on', PORT)
})

